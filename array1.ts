const names: String[] = [];
names.push("Kaewalin");
names.push("Machot");

console.log(names[0]);
console.log(names[1]);
console.log(names.length);

for (let i = 0; i < names.length; i++){
    console.log(names[i]);
}
for (const name in names) {
    console.log(names[name]);
}

names.forEach(function (name) {
    console.log(name);
});

