function printStatusCode(code: string | number) {
    if (typeof code === 'string') {
        console.log(`My status code is ${code.toUpperCase()} ${typeof code}`);
    }else{
        console.log(`My status code is ${code} ${typeof code}`);
    }
}
printStatusCode(400);
printStatusCode("Kaewalin")
